-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 21, 2015 at 11:47 PM
-- Server version: 5.6.26-74.0-log
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flagman`
--
CREATE DATABASE IF NOT EXISTS `flagman` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `flagman`;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` tinyint(3) unsigned NOT NULL,
  `is_section` tinyint(3) unsigned NOT NULL COMMENT 'Тип записи',
  `mpath` varchar(128) NOT NULL COMMENT 'Материальный путь',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `title_ua` varchar(255) NOT NULL COMMENT 'Название (по-украински)',
  `name` varchar(128) NOT NULL COMMENT 'Имя',
  `code` varchar(2) NOT NULL COMMENT 'Код страны',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `parent_id_index` (`parent_id`),
  KEY `is_section_index` (`is_section`),
  KEY `mpath_index` (`mpath`),
  KEY `title_index` (`title`),
  KEY `title_ua_index` (`title_ua`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `parent_id`, `is_section`, `mpath`, `title`, `title_ua`, `name`, `code`) VALUES
(1, 0, 0, '.', 'Украина', 'Україна', 'ukraine', '');

-- --------------------------------------------------------

--
-- Table structure for table `countries_cities`
--

DROP TABLE IF EXISTS `countries_cities`;
CREATE TABLE IF NOT EXISTS `countries_cities` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` mediumint(8) unsigned NOT NULL,
  `is_section` tinyint(3) unsigned NOT NULL COMMENT 'Тип записи',
  `mpath` varchar(128) NOT NULL COMMENT 'Материальный путь',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `title_ua` varchar(255) NOT NULL COMMENT 'Название (по-украински)',
  `name` varchar(128) NOT NULL COMMENT 'Имя',
  `country_id` mediumint(8) unsigned NOT NULL COMMENT 'ID страны',
  `type` tinyint(3) unsigned NOT NULL COMMENT 'Тип',
  `codes` varchar(255) NOT NULL COMMENT 'Код города',
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  `title_prepositional` varchar(255) NOT NULL COMMENT 'Название (предложный)',
  `ext_id` varchar(16) NOT NULL COMMENT 'ID во внешней системе',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Статус',
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_id_parent_id_name_unique` (`country_id`,`parent_id`,`name`),
  KEY `parent_id_index` (`parent_id`),
  KEY `is_section_index` (`is_section`),
  KEY `mpath_index` (`mpath`),
  KEY `title_index` (`title`),
  KEY `title_ua_index` (`title_ua`),
  KEY `order_index` (`order`),
  KEY `title_prepositional_index` (`title_prepositional`),
  KEY `status_index` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `countries_cities`
--

INSERT INTO `countries_cities` (`id`, `parent_id`, `is_section`, `mpath`, `title`, `title_ua`, `name`, `country_id`, `type`, `codes`, `order`, `title_prepositional`, `ext_id`, `status`) VALUES
(21, 0, 0, '.', 'Ивано-Франковск', 'Івано-Франківськ', '21', 1, 4, '', 16777215, '', '', 1),
(13, 0, 0, '.', 'Киев', 'Киів', '13', 1, 4, '', 10, '', '', 1),
(12, 0, 0, '.', 'Запорожье', 'Запоріжжя', '12', 1, 4, '', 60, '', '', 1),
(10, 0, 0, '.', 'Днепропетровск', 'Дніпропетровськ', '10', 1, 4, '', 0, '', '', 1),
(20, 0, 0, '.', 'Ровно', 'Рівне', '20', 1, 4, '', 16777215, '', '', 1),
(16, 0, 0, '.', 'Львов', 'Львів', '16', 1, 4, '', 30, '', '', 1),
(17, 0, 0, '.', 'Одесса', 'Одеса', '17', 1, 4, '', 50, '', '', 1),
(18, 0, 0, '.', 'Харьков', 'Харків', '18', 1, 4, '', 40, '', '', 1),
(19, 0, 0, '.', 'Николаев', 'Миколаїв', '19', 1, 4, '', 16777215, '', '', 1),
(22, 0, 0, '.', 'Винница', 'Вінниця', '22', 1, 4, '', 16777215, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_methods`
--

DROP TABLE IF EXISTS `orders_deliveries_methods`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_methods` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `name` varchar(128) NOT NULL COMMENT 'Имя',
  `commission` decimal(10,2) unsigned DEFAULT NULL COMMENT 'Комиссия',
  `term` varchar(32) NOT NULL COMMENT 'Срок доставки',
  `description` text NOT NULL COMMENT 'Краткое oписание',
  `created` datetime NOT NULL COMMENT 'Дата создания',
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Статус',
  `full_description` text NOT NULL COMMENT 'Полное описание',
  `need_phone` tinyint(3) unsigned NOT NULL COMMENT 'Телефон',
  `need_address` tinyint(3) unsigned NOT NULL COMMENT 'Адрес',
  `full_description_mobile` text NOT NULL COMMENT 'Полное описание (мобильное)',
  `default` tinyint(3) unsigned NOT NULL COMMENT 'Метод доставки по умолчанию',
  `need_place` tinyint(3) unsigned NOT NULL COMMENT 'Самовывоз',
  `currency_id` smallint(5) unsigned DEFAULT NULL COMMENT 'ID валюты комиссии',
  PRIMARY KEY (`id`),
  KEY `created_index` (`created`),
  KEY `order_index` (`order`),
  KEY `status_index` (`status`),
  KEY `title_index` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `orders_deliveries_methods`
--

INSERT INTO `orders_deliveries_methods` (`id`, `title`, `name`, `commission`, `term`, `description`, `created`, `order`, `status`, `full_description`, `need_phone`, `need_address`, `full_description_mobile`, `default`, `need_place`, `currency_id`) VALUES
(1, 'Адресная курьерская доставка', 'courier', 30.00, '3 дня', 'Курьеры наших партнеров доставят товар по указанному вами адресу.', '2008-03-05 13:34:00', 2, 1, '<p>Курьеры наших партнеров доставят товар по указанному вами адресу.</p>', 1, 1, '', 0, 0, NULL),
(2, 'Самовывоз', 'pickup', NULL, '', 'Ваш товар могут привезти в удобный для вас пункт самовывоза в вашем городе.', '2008-03-05 13:34:00', 3, 1, '<p>Ваш товар могут привезти в удобный для вас пункт самовывоза в вашем городе.</p>', 1, 0, '', 1, 1, NULL),
(7, 'Службой доставки «Новая почта»', 'nova-poshta', 25.00, '3 дня', 'Ваш товар могут привезти на указанный вами склад «Новой почты».', '2013-03-14 13:58:12', 1, 1, '<p>Ваш товар могут привезти на указанный вами склад «Новой почты».</p>', 1, 0, '', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_methods_relations`
--

DROP TABLE IF EXISTS `orders_deliveries_methods_relations`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_methods_relations` (
  `name` enum('cities','payment_methods','places','services') NOT NULL COMMENT 'Имя',
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `record_from_id` mediumint(8) unsigned NOT NULL,
  `record_to_id` mediumint(8) unsigned NOT NULL,
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  `relationmanagment_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_record_to_id_record_from_id_unique` (`name`,`record_to_id`,`record_from_id`),
  KEY `record_from_id_index` (`record_from_id`),
  KEY `record_to_id_index` (`record_to_id`),
  KEY `order_index` (`order`),
  KEY `relationmanagment_id_index` (`relationmanagment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `orders_deliveries_methods_relations`
--

INSERT INTO `orders_deliveries_methods_relations` (`name`, `id`, `record_from_id`, `record_to_id`, `order`, `relationmanagment_id`) VALUES
('cities', 96, 1, 12, 4, NULL),
('payment_methods', 13, 2, 14, 0, NULL),
('cities', 23, 2, 16, 0, NULL),
('cities', 98, 1, 10, 6, NULL),
('cities', 28, 7, 17, 1, NULL),
('cities', 27, 7, 18, 0, NULL),
('cities', 99, 1, 17, 7, NULL),
('payment_methods', 10, 7, 14, 0, NULL),
('cities', 100, 1, 18, 8, NULL),
('payment_methods', 12, 1, 14, 0, NULL),
('cities', 95, 1, 13, 3, NULL),
('cities', 92, 1, 16, 0, NULL),
('cities', 29, 7, 16, 2, NULL),
('cities', 32, 7, 13, 5, NULL),
('cities', 33, 7, 12, 6, NULL),
('cities', 35, 7, 10, 8, NULL),
('services', 71, 2, 4, 0, NULL),
('payment_methods', 53, 1, 20, 1, NULL),
('payment_methods', 54, 1, 23, 2, NULL),
('payment_methods', 55, 2, 23, 2, NULL),
('payment_methods', 56, 7, 23, 2, NULL),
('payment_methods', 44, 2, 20, 1, NULL),
('payment_methods', 45, 7, 20, 1, NULL),
('services', 72, 2, 3, 1, NULL),
('cities', 80, 2, 10, 3, NULL),
('cities', 70, 2, 13, 2, NULL),
('services', 87, 1, 3, 0, NULL),
('services', 88, 1, 4, 1, NULL),
('services', 90, 7, 5, 0, NULL),
('', 101, 2, 5, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_services`
--

DROP TABLE IF EXISTS `orders_deliveries_services`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_services` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `contact_name` varchar(255) NOT NULL COMMENT 'Контактное лицо',
  `contact_phone` bigint(20) unsigned NOT NULL COMMENT 'Контактный номер',
  `address` varchar(255) NOT NULL COMMENT 'Адрес представительства в городе магазина',
  PRIMARY KEY (`id`),
  KEY `title_index` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `orders_deliveries_services`
--

INSERT INTO `orders_deliveries_services` (`id`, `title`, `contact_name`, `contact_phone`, `address`) VALUES
(2, 'test', '1', 1, '1'),
(3, 'Пункт самовывоза Demo', 'Гермес Конрад', 0, 'Днепропетровск. Артельная, 9'),
(4, 'Пункт самовывоза DemoMultichannel', 'Бендер Родригес', 0, 'Киев, Грушевского, 3'),
(5, 'Новая служба', '11', 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_services_places`
--

DROP TABLE IF EXISTS `orders_deliveries_services_places`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_services_places` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `city_id` mediumint(8) unsigned NOT NULL COMMENT 'ID города',
  `service_id` smallint(5) unsigned NOT NULL,
  `address` varchar(255) NOT NULL COMMENT 'Адрес',
  `url` varchar(255) NOT NULL COMMENT 'Ссылка на расположение склада на яндекс-картах',
  `longitude` decimal(9,6) unsigned DEFAULT NULL COMMENT 'Долгота',
  `latitude` decimal(8,6) unsigned DEFAULT NULL COMMENT 'Широта',
  `phones` varchar(255) NOT NULL COMMENT 'Контактные номера телефонов',
  `operating_schedule` text NOT NULL COMMENT 'График работы',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Статус',
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  PRIMARY KEY (`id`),
  KEY `title_index` (`title`),
  KEY `service_id_index` (`service_id`),
  KEY `status_index` (`status`),
  KEY `order_index` (`order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `orders_deliveries_services_places`
--

INSERT INTO `orders_deliveries_services_places` (`id`, `title`, `city_id`, `service_id`, `address`, `url`, `longitude`, `latitude`, `phones`, `operating_schedule`, `status`, `order`) VALUES
(4, 'Пункт самовывоза Demo, Днепропетровск, ул. Артельная, 9', 10, 3, 'ул. Артельная, 9', 'фыф', 35.071429, 48.487814, '', 'Будние дни с 9:00 до 19:00. Сб–Вс — выходные дни', 1, 0),
(5, 'Пункт самовывоза Demo, Днепропетровск, пр. Воронцова, 64', 10, 3, 'пр. Воронцова, 64', 'фыв', 35.056543, 48.489466, '', 'Граффик работы: 8:00 - 20:00.\rВыходные: сб, вс', 1, 0),
(6, 'Пункт самовывоза Demo, Киев, Крещатик 27', 13, 3, 'Крещатик 27', 'фыв', 30.521712, 50.444121, '', 'Пн - пт. С 9:00-20:00\rСб. 10:00 - 16:00\rВс выходной', 1, 0),
(7, 'Пункт самовывоза Demo, Днепропетровск, пр. Калинина, 42', 10, 3, 'пр. Калинина, 42', '', 34.996050, 48.475547, '+38(066)111-11-11', 'Время работы: с 9:00 до 21:00, ежедневно', 1, 0),
(9, 'Пункт самовывоза Demo, Днепропетровск, ул. Рабочая, 148', 10, 3, 'ул. Рабочая, 148', '', 35.002549, 48.452702, '', 'Время работы: с 11:00 до 19:00, ежедневно', 1, 0),
(10, 'Пункт самовывоза Demo, Днепропетровск, ул. Строителей, 19а', 10, 3, 'ул. Строителей, 19а', '', 35.004552, 48.423778, '', 'Пн-Пт: с 9:00 до 19:00, Сб: с 11:00 до 16:00, Вс: выходной', 1, 0),
(11, 'Пункт самовывоза DemoMultichannel, Донецк, ул. Овнатаняна, 20а', 11, 4, 'ул. Овнатаняна, 20а', '', 37.837176, 48.007539, '', '', 1, 0),
(12, 'Пункт самовывоза DemoMultichannel, Днепропетровск, пр. Карла Маркса, 75', 10, 4, 'пр. Карла Маркса, 75', '', 35.039967, 48.466549, '', '', 1, 0),
(13, 'Пункт самовывоза DemoMultichannel, Днепропетровск, ул. Титова, 28', 10, 4, 'ул. Титова, 28', '', 35.004544, 48.432510, '(066)111-11-12', 'Время работы: с 11:00 до 19:00, ежедневно', 1, 0),
(15, 'Новая служба, Львов, Новая Почта №3', 16, 5, 'Новая Почта №3', '', 24.049212, 49.809580, '+38 (032) 290-19-11', '', 1, 16777215);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
