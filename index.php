<?php
require_once 'bootstrap.php';

$country = [
	'uuid' => 'C35B6195-4EA3-11DE-8591-001D600938F8',
	'title_ru' => 'Украина',
	'title_ua' => 'Україна',
	'name' => 'ukraine',
	'code' => 'UA'
];
$service = [
	'contact' => 'Торговый Дом «Мист Экспресс»',
	'title' => 'Мист Экспресс',
	'phone' => 380504327707,
	'address' => 'Киев'
];
$method = [
	'title' => 'Мист Экспресс',
	'name' => 'meest-express',
	'order' => 1,
	'default' => 0
];
$meestExpress = new MeestExpress();
$meestExpress->copyWarehouses($country, $method, $service);


$country = [
	'title_ru' => 'Украина',
	'title_ua' => 'Україна',
	'name' => 'ukraine',
	'code' => 'UA'
];
$service = [
	'contact' => 'ООО «Новая Почта»',
	'title' => 'Новая Почта',
	'phone' => '0800500609',
	'address' => 'Киев'
];
$method = [
	'title' => 'Новая Почта',
	'name' => 'novaposhta',
	'order' => 2,
	'default' => 1
];

$novaPoshta = new NovaPoshta();
$novaPoshta->copyWarehouses($country, $method, $service);