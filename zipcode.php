<?php
require_once 'bootstrap.php';
$DB = new DB();
$DB->truncate('_regions');
$DB->truncate('_cities');
$DB->truncate('_districts');
for ($i = 1; $i <= 27; $i ++) {
	$url = 'http://www.zipcode.com.ua/ind/' . str_pad($i, 2, '0', STR_PAD_LEFT) . '.shtml';
	$hash = md5($url);
	$filename =  SITE_DIR . DS . 'cache' . DS . 'zipcode' . DS . $hash;
	if (!is_file($filename)) {
		if ($html = file_get_contents($url)) {
			$html = iconv('windows-1251', 'UTF-8', str_replace(array("\n", "\r"), ' ', $html));
			file_put_contents($filename, $html);
		}
	}
	else {
		$html = file_get_contents($filename);
	}
	if (preg_match('/<HR size="1" color="#000000">(.+?)<HR size="1" color="#000000">/ui', $html, $matches)) {
		$title = trim(strip_tags(str_replace(array('АВТОНОМНА РЕСПУБЛІКА', 'ОБЛАСТЬ'), '', $matches[1])));
		$title = mb_convert_case(mb_strtolower($title), MB_CASE_TITLE, 'UTF-8');
	}
	if ($title == 'Київ, Місто' || $title == 'Севастополь, Місто') {
		continue;
	}
	$region_id = saveRegion($title);
	if (preg_match_all('/<A href="(.+?)" class="regions" target="_blank">(.+?)<\/A>/ui', $html, $matches)) {
		foreach ($matches[1] as $k => $uri) {
			$url = 'http://www.zipcode.com.ua/ind/' . $uri;
			$hash = md5($url);
			$filename =  SITE_DIR . DS . 'cache' . DS . 'zipcode' . DS . $hash;
			if (!is_file($filename)) {
				if ($html = file_get_contents($url)) {
					$html = iconv('windows-1251', 'UTF-8', str_replace(array("\n", "\r"), ' ', $html));
					file_put_contents($filename, $html);
				}
			}
			else {
				$html = file_get_contents($filename);
			}
			$title = trim(strip_tags(str_replace(array('район', ',', 'місто', '`', "'"), array('', '', '', '’', '’'), $matches[2][$k])));
			$title = preg_replace('/\s+/ui', ' ', $title);
			$title = str_replace(array('i', 'c'), array('і', 'с'), $title);
			$district_id = saveDistrict($title, $region_id);
			if (preg_match_all('/<TD>([^<]+?)<\/TD>\s+<TD width="78">/ui', $html, $matches2)) {
				foreach ($matches2[1] as $city) {
					$city = trim(strip_tags(str_replace(array('`', "'"), '’', $city)));
					$city = preg_replace('/\s+/ui', ' ', $city);
					$city = str_replace(array('i', 'c'), array('і', 'с'), $city);
					saveCity($city, $region_id, $district_id);
				}
			}
			
		}
	}
}
if (($handle = fopen(SITE_DIR . DS . 'data' . DS . 'zipcode' . DS . 'cities.csv', 'r')) !== false) {
	while (($data = fgetcsv($handle, 1000, ';')) !== false) {
		$region_id = saveRegion($data[0]);
		$district_id = saveDistrict($data[1], $region_id);
		saveCity($data[2], $region_id, $district_id);
	}
	fclose($handle);
}
function saveRegion($title) {
	global $DB;
	$conditions = [
		'title' => $title
	];
	$result = $DB->select('_regions', $conditions);
	if (empty($result)) {
		$fields = $conditions;
		return $DB->insert('_regions', $fields);
	}
	return $result[0]['id'];
}
function saveDistrict($title, $region_id) {
	global $DB;
	$conditions = [
		'title' => $title,
		'region_id' => $region_id
	];
	$result = $DB->select('_districts', $conditions);
	if (empty($result)) {
		$fields = $conditions;
		return $DB->insert('_districts', $fields);
	}
	return $result[0]['id'];
}
function saveCity($title, $region_id, $district_id) {
	global $DB;
	$conditions = [
		'title' => $title,
		'region_id' => $region_id,
		'district_id' => $district_id
	];
	$result = $DB->select('_cities', $conditions);
	if (empty($result)) {
		$fields = $conditions;
		return $DB->insert('_cities', $fields);
	}
	return $result[0]['id'];
}