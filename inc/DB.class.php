<?php
class DB {
	protected $PDO;
	protected $logger;
	protected $log_queries = true;
	public function __construct() {
		$config = Config::getInstance();
		$dsn = 'mysql:dbname=' . $config->getValue('db.dbname') . ';host=' . $config->getValue('db.host');
		$this->PDO = new PDO($dsn, $config->getValue('db.user'), $config->getValue('db.password'), [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'']);
		if ($this->log_queries) {
			$this->logger = new Logger('DB', false);
		}
	}
	public function truncate($table) {
		return $this->PDO->exec('TRUNCATE TABLE ' . $table);
	}
	public function insert($table, $fields = []) {
		$params = $params_values = [];
		if (!empty($fields)) {
			$sql = 'INSERT INTO ' . $table . '(`' . implode('`,`', array_keys($fields)) . '`)';
			foreach ($fields as $key => $value) {
				$params[] = ':' . $key;
				$params_values[':' . $key] = $value;
			}
			$sql .= ' VALUES(' . implode(',', $params) . ')';
		}
		else {
			$sql = 'INSERT INTO ' . $table;
		}
		$sth = $this->PDO->prepare($sql);
		$this->logger->log($sth->queryString);
		if (!empty($params_values)) {
			$this->logger->log($params_values);
		}
		if (!$sth->execute($params_values)) {
			$this->logger->log($sth->errorInfo());
		}
		return $this->PDO->lastInsertId();
	}
	public function update() {
	}
	public function delete() {
		//$count = $this->PDO->exec('DELETE FROM fruit WHERE colour = 'red'');
	}
	public function select($table, $conditions = [], $fields = ['*'], $order = []) {
		$params = $params_values = [];
		if ($fields[0] == '*') {
			$sql = 'SELECT * FROM ' . $table;
		}
		else {
			$sql = 'SELECT `' . implode('`,`', $fields) . '` FROM ' . $table;
		}
		if (!empty($conditions)) {
			foreach ($conditions as $key => $value) {
				if (is_array($value)) {
					$_params = [];
					foreach ($value as $i => $val) {
						$_key = ':' . $key . $i;
						$_params[] = $_key;
						$params_values[$_key] = $val;
					}
					$params[] = $key . ' IN(' . implode(',', $_params) . ')';
				}
				else {
					$params[] = $key . '=:' . $key;
					$params_values[':' . $key] = $value;
				}
			}
			$sql .= ' WHERE ' . implode(' AND ', $params);
		}
		$sth = $this->PDO->prepare($sql);
		$this->logger->log($sth->queryString);
		if (!empty($params_values)) {
			$this->logger->log($params_values);
		}
		if (!$sth->execute($params_values)) {
			$this->logger->log($sth->errorInfo());
			return false;
		}
		else {
			return $sth->fetchAll();
		}
	}
}