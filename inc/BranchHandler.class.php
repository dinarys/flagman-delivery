<?php
class BranchHandler extends DefaultHandler {
	protected $formatters = [
		'Branchtype' => 'branchType',
		'WorkingHours' => 'workinHours',
		'Schedule' => 'schedule'
	];
	protected $weekdays = [
		'Monday' => 'Пн',
		'Tuesday' => 'Вт',
		'Wednesday' => 'Ср',
		'Thursday' => 'Чт',
		'Friday' => 'Пт',
		'Saturday' => 'Сб',
		'Sunday' => 'Вс'
	];
	protected function branchTypeFormatter($value) {
		return $value === 'Приват-АПТ' ? true : false;
	}
	protected function workinHoursFormatter($value) {
		$week = [];
		$days = array_map('trim', explode(';', $value));
		foreach ($days as $day) {
			$dayname = mb_substr($day, 0, 2);
			if ($dayname == 'Нд') {
				$dayname = 'Вс';
			}
			$week[$dayname] = mb_substr($day, 3);
		}
		$parts = [];
		foreach ($week as $day => $hours) {
			if (isset($prev_hours)) {
				if ($prev_hours != $hours) {

					if ($start_day != $end_day) {
						$parts[] = $start_day . '-' . $end_day . ': ' . $this->convertTime($prev_hours);
					}
					else {
						$parts[] = $start_day . ': ' . $this->convertTime($prev_hours);
					}
					$start_day = $end_day = $day;
				}
				else {
					$end_day = $day;
				}
			}
			else {
				$start_day = $end_day = $day;
			}
			$prev_hours = $hours;
		}
		if ($start_day != $day) {
			$parts[] = $start_day . '-' . $day . ': ' . $this->convertTime($hours);
		}
		else {
			$parts[] = $day . ': ' . $this->convertTime($hours);
		}
		return implode(', ', $parts);
	}
	protected function convertTime($time) {
		if ($time == '--:-- - --:--') {
			return 'выходной';
		}
		else {
			$parts = explode(' - ', $time);
			return 'с ' . $parts[0] . ' до ' . $parts[1];
		}
	}
	protected function convertTimeNP($time) {
		if ($time == '-') {
			return 'выходной';
		}
		else {
			$parts = explode('-', $time);
			return 'с ' . $parts[0] . ' до ' . $parts[1];
		}
	}
	protected function scheduleFormatter($value) {
		$week = [];
		foreach ($value as $day => $val) {
			$dayname = $this->weekdays[$day];
			$week[$dayname] = $val;
		}
		$parts = [];
		foreach ($week as $day => $hours) {
			if (isset($prev_hours)) {
				if ($prev_hours != $hours) {

					if ($start_day != $end_day) {
						$parts[] = $start_day . '-' . $end_day . ': ' . $this->convertTimeNP($prev_hours);
					}
					else {
						$parts[] = $start_day . ': ' . $this->convertTimeNP($prev_hours);
					}
					$start_day = $end_day = $day;
				}
				else {
					$end_day = $day;
				}
			}
			else {
				$start_day = $end_day = $day;
			}
			$prev_hours = $hours;
		}
		if ($start_day != $day) {
			$parts[] = $start_day . '-' . $day . ': ' . $this->convertTimeNP($hours);
		}
		else {
			$parts[] = $day . ': ' . $this->convertTimeNP($hours);
		}
		return implode(', ', $parts);
	}
}