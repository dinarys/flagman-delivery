<?php
class NovaPoshtaAPI {
	protected $path = 'novaposhta';
	protected $data_path;
	protected $cached;
	public function __construct() {
		$this->data_path = SITE_DIR . DS . 'data' . DS . $this->path . DS;
	}
	public function request($function, $ref = null) {
		if (isset($cached[$functiuon])) {
			$result = $cached[$functiuon];
		}
		else {
			switch ($function) {
				case 'Region':
					$filename = 'areas.json';
					break;
				case 'City':
					$filename = 'cities.json';
					break;
				case 'Branch':
					$filename = 'warehouses.json';
				
					break;
			}
			if (isset($filename) && is_file($this->data_path . $filename) && ($json = file_get_contents($this->data_path . $filename)) !== false && ($decoded = json_decode($json, true)) !== false) {
				$result = $cached[$functiuon] = $decoded['data'];
			}
		}
		if (isset($result)) {
			if (!empty($ref)) {
				return $this->filter($result, $function, $ref);
			}
			return $result;
		}
		return false;
	}
	protected function filter($result, $function, $ref) {
		switch ($function) {
			case 'City':
				$key = 'Area';
				break;
			case 'Branch':
				$key = 'CityRef';
				break;
		}
		if (isset($key)) {
			foreach ($result as $item) {
				if (isset($item[$key]) && $item[$key] == $ref) {
					yield $item;
				}
			}
		}
	}
}