<?php
class Logger {
	protected $fh;
	protected $log = false;
	public function __construct($filename, $append = true) {
		if ($this->fh = fopen(SITE_DIR . DS . 'logs' . DS . $filename . '.log', $append ? 'a' : 'w')) {
			$this->log = true;
		}
	}
	public function log($data) {
		if ($this->log) {
			if (is_array($data) || is_object($data)) {
				$line = print_r($data, true);
			}
			else {
				$line = $data;
			}
			fwrite($this->fh, $line . PHP_EOL);
		}
	}
	public function __destruct() {
		if ($this->log) {
			fclose($this->fh);
		}
	}
}