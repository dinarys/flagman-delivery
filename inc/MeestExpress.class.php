<?php
class MeestExpress {
	protected $db;
	protected $api;
	protected $clean = false;
	protected $order = [];
	public function __construct() {
		$this->db = new DB();
		$this->api = new MeestExpressAPI();
		$config = Config::getInstance();
		$this->clean = $config->getValue('meest_express.clean');
		if ($this->clean) {
			$this->cleanDB();
		}
	}
	public function cleanDB() {
		$this->db->truncate('countries');
		$this->db->truncate('countries_cities');
		$this->db->truncate('orders_deliveries_methods');
		$this->db->truncate('orders_deliveries_methods_relations');
		$this->db->truncate('orders_deliveries_services');
		$this->db->truncate('orders_deliveries_services_places');
	}
	public function copyWarehouses($country, $delivery_method, $delivery_service) {
		// add country
		// country_id
		$country_id = $this->saveCountry($country);
		// add delivery_method
		// delivery_method_id
		$delivery_method_id = $this->saveDeliveryMethod($delivery_method);
		// add delivery_service
		// delivery_service_id
		$delivery_service_id = $this->saveDeliveryService($delivery_service);
		// add method-service relation
		$this->saveDeliveryMethodService($delivery_method_id, $delivery_service_id);

		$regions = $this->getRegions($country['uuid']);
		foreach ($regions as $region_uuid => $region) {
			// add/update region
			// region_id
			$region_id = $this->saveRegion($country_id, $region);
			$districts = $this->getDistricts($region_uuid);
			foreach ($districts as $district_uuid => $district) {
				// add/update district
				// district_id
				$district_id = $this->saveDistrict($country_id, $region_id, $district);
				// get cities
				$cities = $this->getCities($district_uuid);
				foreach ($cities as $city_uuid => $city) {
					// add/update city
					// city_id
					$city_id = $this->saveCity($country_id, $region_id, $district_id, $city);
					// get branches
					$branches = $this->getBranches($city_uuid);
					foreach ($branches as $branch_uuid => $branch) {
						// add/update branch
						$this->saveBranch($city_id, $delivery_service_id, $delivery_service, $city, $branch);
					}
				}
			}
		
		}
	}
	protected function saveCountry($country) {
		$conditions = [
			'title' => $country['title_ru'],
			'title_ua' => $country['title_ua']
		];
		$result = $this->db->select('countries', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['code'] = $country['code'];
			$fields['name'] = $country['name'];
			$fields['mpath'] = '.';
			return $this->db->insert('countries', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveDeliveryMethod($delivery_method) {
		$conditions = [
			'title' => $delivery_method['title'],
			'name' => $delivery_method['name']
		];
		$result = $this->db->select('orders_deliveries_methods', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['order'] = $delivery_method['order'];
			$fields['default'] = $delivery_method['default'];
			$fields['created'] = date('Y-m-d H:i:s');
			$fields['status'] = 1;
			$fields['need_phone'] = 1;
			$fields['need_place'] = 1;
			return $this->db->insert('orders_deliveries_methods', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveDeliveryService($delivery_service) {
		$conditions = [
			'title' => $delivery_service['title']
		];
		$result = $this->db->select('orders_deliveries_services', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['contact_name'] = $delivery_service['contact'];
			$fields['contact_phone'] = $delivery_service['phone'];
			$fields['address'] = $delivery_service['address'];
			return $this->db->insert('orders_deliveries_services', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveDeliveryMethodService($delivery_method_id, $delivery_service_id) {
		$conditions = [
			'record_from_id' => $delivery_method_id,
			'record_to_id' => $delivery_service_id,
			'name' => 'services'
		];
		$result = $this->db->select('orders_deliveries_methods_relations', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['order'] = 0;
			return $this->db->insert('orders_deliveries_methods_relations', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveRegion($country_id, $region) {
		$conditions = [
			'parent_id' => 0,
			'is_section' => 1,
			'country_id' => $country_id,
			'title' => $region['title_ru'],
			'title_ua' => $region['title_ua']
		];
		$result = $this->db->select('countries_cities', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['name'] = String::str2slug($region['title_ru']);
			$fields['mpath'] = '.';
			$fields['type'] = 0;
			$fields['status'] = 1;
			$fields['order'] = $this->getOrder('region');
			return $this->db->insert('countries_cities', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveDistrict($country_id, $region_id, $district) {
		$conditions = [
			'parent_id' => $region_id,
			'is_section' => 1,
			'country_id' => $country_id,
			'title' => $district['title_ru'],
			'title_ua' => $district['title_ua']
		];
		$result = $this->db->select('countries_cities', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['name'] = String::str2slug($district['title_ru']);
			$fields['mpath'] = '.' . $region_id . '.';
			$fields['type'] = 0;
			$fields['status'] = 1;
			$fields['order'] = $this->getOrder('district');
			return $this->db->insert('countries_cities', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveCity($country_id, $region_id, $district_id, $city) {
		$conditions = [
			'parent_id' => $district_id,
			'is_section' => 0,
			'country_id' => $country_id,
			'title' => $city['title_ru'],
			'title_ua' => $city['title_ua']
		];
		$result = $this->db->select('countries_cities', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['name'] = String::str2slug($city['title_ru']);
			$fields['mpath'] = '.' . $region_id . '.' . $district_id . '.';
			$fields['type'] = 3;
			$fields['status'] = 1;
			$fields['order'] = $this->getOrder('city');
			return $this->db->insert('countries_cities', $fields);
		}
		return $result[0]['id'];
	}
	protected function saveBranch($city_id, $service_id, $service, $city, $branch) {
		$address = '';
		if ($branch['branch_type']) {
			$address = 'АПТ №' . $branch['branch_code'] . ' (Отделение ПриватБанка), ';
		}
		$address .= $branch['street_type_ru'] . ' ' . $branch['street_ru'] . ', ';
		$address .= $branch['house'];
		$title = $service['title'] . ', ' . $city['title_ru'] . ', ' . $address;
		$conditions = [
			'city_id' => $city_id,
			'service_id' => $service_id,
			'title' => $title,
			'address' => $address
		];
		$result = $this->db->select('orders_deliveries_services_places', $conditions);
		if (empty($result)) {
			$fields = $conditions;
			$fields['longitude'] = $branch['longitude'];
			$fields['latitude'] = $branch['latitude'];
			if (isset($branch['working_hours'])) {
				$fields['operating_schedule'] = $branch['working_hours'];
			}
			$fields['status'] = 1;
			$fields['order'] = $this->getOrder('branch');
			return $this->db->insert('orders_deliveries_services_places', $fields);
		}
		return $result[0]['id'];
	}
	protected function convertResult($items, $handler = 'DefaultHandler') {
		if (!is_null($items)) {
			$handler = new $handler();
			if (isset($items['uuid']) || isset($items['UUID'])) {
				$items = [$items];
			}
			foreach ($items as $item) {
				$uuid = 'uuid';
				if (isset($item['UUID'])) {
					$uuid = 'UUID';
				}
				yield $item[$uuid] => $handler->handle($item);
			}
		}
	}
	protected function getRegions($country_uuid) {
		$conditions = [
			'Countryuuid' => $country_uuid
		];
		return $this->convertResult($this->api->request('Region', $conditions), 'RegionHandler');
	}
	protected function getDistricts($region_uuid) {
		$conditions = [
			'Regionuuid' => $region_uuid
		];
		return $this->convertResult($this->api->request('District', $conditions));
	}
	protected function getCities($district_uuid) {
		$conditions = [
			'Districtuuid' => $district_uuid,
			'IsBranchInCity' => 1
		];
		return $this->convertResult($this->api->request('City', $conditions), 'CityHandler');
	}
	protected function getBranches($city_uuid) {
		$conditions = [
			'CityUUID' => $city_uuid
		];
		return $this->convertResult($this->api->request('Branch', $conditions), 'BranchHandler');
	}
	protected function getOrder($type) {
		if (!isset($this->order[$type])) {
			$this->order[$type] = 10;
		}
		else {
			$this->order[$type] +=10;
		}
		return $this->order[$type];
	}
}