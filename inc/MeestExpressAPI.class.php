<?php
class MeestExpressAPI {
	protected $login;
	protected $password;
	protected $cache;
	protected $order = '';
	public function __construct() {
		$config = Config::getInstance();
		$this->login = $config->getValue('meest_express.login');
		$this->password = $config->getValue('meest_express.password');
		$this->cache = new Cache('meest', '', 86400);
	}
	protected function signature($function, $where) {
		return md5($this->login . $this->password . $function . $where . $this->order);
	}
	protected function where($conditions) {
		$statements = [];
		foreach ($conditions as $key => $value) {
			$statements[] = $key . '="' . $value . '"';
		}
		return implode(' AND ', $statements);
	}
	public function request($function, $conditions) {
		$where = $this->where($conditions);
		$signature = $this->signature($function, $where);
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<param>
			<login>' . $this->login .'</login>
			<function>' . $function .'</function>
			<where>' . $where .'</where>
			<order>' . $this->order .'</order>
			<sign>' . $signature .'</sign>
			</param>';
		$cache_key = md5($xml);
		$response = $this->cache->read($cache_key);
		if ($response === false) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://api1c.meest-group.com/services/1C_Query.php');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/xml']);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch);
			$this->cache->write($cache_key, $response);
		}
		if ($result = simplexml_load_string($response)) {
			if ($result->errors->code == '000') {
				return $this->xml2array($result->result_table)['items'];
			}
		}
		return false;
	}
	private function xml2array($xmlObject, $out = array()) {
		foreach ((array)$xmlObject as $index => $node) {
			$value = (!empty($node) && (is_object($node) || is_array($node))) ? $this->xml2array($node) : $node;
			if (!is_array($value)) {
				$value = trim($value);
			}
			if (!empty($value)) {
				$out[$index] = $value;
			}
		}
		return $out;
	}
}