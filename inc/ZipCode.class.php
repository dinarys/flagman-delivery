<?php
class ZipCode {
	protected $db;
	public function __construct() {
		$this->db = new DB();
	}
	public function getDistrict($region, $city) {
		list($city, $district) = explode(', ', $city);
		$conditions = [
			'region_id' => $this->getRegion($region),
			'title' => $city
		];
		$result = $this->db->select('_cities', $conditions);
		$district_ids = [];
		foreach ($result as $item) {
			$district_ids[] = $item['district_id'];
		}
		$conditions = [
			'id' => $district_ids
		];
		if (!empty($district)) {
			$conditions['title'] = $district;
		}
		$result = $this->db->select('_districts', $conditions);
		if (!empty($result)) {
			return $result[0]['title'];
		}
		return false;
	}
	protected function getRegion($region) {
		$conditions = [
			'title' => $region
		];
		$result = $this->db->select('_regions', $conditions);
		if (empty($result)) {
			echo $region;
		}
		return $result[0]['id'];
	}
}