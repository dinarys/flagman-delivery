<?php
class Cache {
	protected $path;
	protected $prefix;
	protected $cache_lifetime = 3600;
	protected $cache_path;
	public function __construct($path = '', $prefix = '', $cache_lifetime = 3600) {
		$this->path = $path;
		$this->prefix = $prefix;
		$this->cache_lifetime = $cache_lifetime;
		$this->cache_path = SITE_DIR . DS . 'cache' . DS . (!empty($this->path) ? $this->path . DS : '') . $this->prefix;
	}
	public function write($key, $data) {
		file_put_contents($this->cache_path . $key, $data);
	}
	public function read($key) {
		if ($this->check($key)) {
			return file_get_contents($this->cache_path . $key);
		}
		return false;
	}
	public function check($key) {
		$filepath = $this->cache_path . $key;
		if (is_file($filepath) && (filemtime($filepath) + $this->cache_lifetime) > time()) {
			return true;
		}
		return false;
	}
	public function __destruct() {
		clearstatcache();
	}
}