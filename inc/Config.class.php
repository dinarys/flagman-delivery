<?php
class Config {
	private $settings;
	private static $instance;

	private function __construct() {
	}
	private function loadSettings() {
		$path = SITE_DIR . DS . 'config' . DS . 'settings.ini';
		$this->settings = parse_ini_file($path, true);
	}
	public static function getInstance() {
		if (empty(self::$instance)) {
			self::$instance = new Config();
			self::$instance->loadSettings();
		}
		return self::$instance;
	}

	public function getValue($key) {
		$path = explode('.', $key);
		$_settings = $this->settings;
		foreach ($path as $key) {
			if (isset($_settings[$key])) {
				$_settings = $_settings[$key];
			}
			else {
				return null;
			}
		}
		return $_settings;
	}
}