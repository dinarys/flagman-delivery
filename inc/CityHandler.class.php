<?php
class CityHandler extends DefaultHandler {
	protected $formatters = [
		'Description' => 'area',
		'DescriptionRu' => 'area',
		'DescriptionUA' => 'area'
	];
	protected function areaFormatter($value) {
		$postfix = '';
		if (preg_match('/(\(.+\))/ui', $value, $matches)) {
			if (preg_match('/([\w\'-]+)\sр-н/ui', $matches[1], $matches2)) {
				$postfix = ', ' . $matches2[1];
			}
		}
		switch ($value) {
			case 'Берегове':
				$value = 'Берегово';
				break;
			case 'Яремче':
				$value = 'Яремча';
				break;
			case 'Тернопіль (с. Біла)':
				$value = 'Біла';
				break;
		}
		return $this->apostropheFormatter(trim(preg_replace('/\(.+\)/ui', '', $value)) . $postfix);
	}
}