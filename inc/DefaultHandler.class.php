<?php
class DefaultHandler implements Handler {
	protected $fieldsMap = [
		'DescriptionUA' => 'title_ua',
		'DescriptionRU' => 'title_ru',
		'DescriptionRu' => 'title_ru',
		'StreetDescriptionUA' => 'street_ua',
		'StreetDescriptionRU' => 'street_ru',
		'House' => 'house',
		'Flat' => 'flat',
		'Latitude' => 'latitude',
		'Longitude' => 'longitude',
		'StreetTypeRU' => 'street_type_ru',
		'StreetTypeUA' => 'street_type_ua',
		'AddressMoreInformation' => 'address_more_information',
		'BranchCode' => 'branch_code',
		'WorkingHours' => 'working_hours',
		'Branchtype' => 'branch_type',
		'Description' => 'title_ua',
		'AreasCenter' => 'region_center',
		'Schedule' => 'schedule'
	];
	protected $formatters = [
		'Description' => 'apostrophe',
		'DescriptionUA' => 'apostrophe'
	];
	public function handle($item) {
		return iterator_to_array($this->handleFields($item));
	}
	private function handleFields($item) {
		foreach ($item as $key => $value) {
			if (isset($this->fieldsMap[$key])) {
				yield $this->fieldsMap[$key] => $this->formatField($key, $value);
			}
		}
	}
	private function formatField($key, $value) {
		if (isset($this->formatters[$key])) {
			$value = $this->{$this->formatters[$key] . 'formatter'}($value);
		}
		return $value;
	}
	protected function apostropheFormatter($value) {
		$value = str_replace(array('i', 'c'), array('і', 'с'), $value);
		return preg_replace('/\s+/ui', ' ', str_replace(array('`', "'"), '’', $value));
	}
}