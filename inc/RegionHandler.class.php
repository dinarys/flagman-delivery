<?php
class RegionHandler extends DefaultHandler {
	protected $formatters = [
		'DescriptionUA' => 'ucwords',
		'DescriptionRU' => 'ucwords',
		'Description' => 'translate'
	];
	protected $translate = [
		'АРК' => 'Крим'
	];
	protected function ucwordsFormatter($value) {
		return $this->apostropheFormatter(mb_convert_case(mb_strtolower($value), MB_CASE_TITLE, 'UTF-8'));
	}
	protected function translateFormatter($value) {
		return $this->apostropheFormatter(isset($this->translate[$value]) ? $this->translate[$value] : $value);
	}
}