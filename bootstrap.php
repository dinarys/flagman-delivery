<?php
define('SITE_DIR', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);

function __autoload($className) {
	$paths = [
		SITE_DIR . DS . 'inc' . DS . $className . '.class.php',
		SITE_DIR . DS . 'inc' . DS . $className . '.interface.php'
	];
	foreach ($paths as $path) {
		if (file_exists($path)) {
			require_once $path;
			return true;
		}
	}
	return false; 
}