-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 17, 2015 at 05:29 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flagman`
--
CREATE DATABASE IF NOT EXISTS `flagman` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `flagman`;

-- --------------------------------------------------------

-- --------------------------------------------------------


DROP TABLE IF EXISTS `countries_cities`;
CREATE TABLE IF NOT EXISTS `countries_cities` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` mediumint(8) unsigned NOT NULL,
  `is_section` tinyint(3) unsigned NOT NULL COMMENT 'Тип записи',
  `mpath` varchar(128) NOT NULL COMMENT 'Материальный путь',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `title_ua` varchar(255) NOT NULL COMMENT 'Название (по-украински)',
  `name` varchar(128) NOT NULL COMMENT 'Имя',
  `country_id` mediumint(8) unsigned NOT NULL COMMENT 'ID страны',
  `type` tinyint(3) unsigned NOT NULL COMMENT 'Тип',
  `codes` varchar(255) NOT NULL COMMENT 'Код города',
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  `title_prepositional` varchar(255) NOT NULL COMMENT 'Название (предложный)',
  `ext_id` varchar(16) NOT NULL COMMENT 'ID во внешней системе',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Статус',
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_id_parent_id_name_unique` (`country_id`,`parent_id`,`name`),
  KEY `parent_id_index` (`parent_id`),
  KEY `is_section_index` (`is_section`),
  KEY `mpath_index` (`mpath`),
  KEY `title_index` (`title`),
  KEY `title_ua_index` (`title_ua`),
  KEY `order_index` (`order`),
  KEY `title_prepositional_index` (`title_prepositional`),
  KEY `status_index` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_methods`
--

DROP TABLE IF EXISTS `orders_deliveries_methods`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_methods` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `name` varchar(128) NOT NULL COMMENT 'Имя',
  `commission` decimal(10,2) unsigned DEFAULT NULL COMMENT 'Комиссия',
  `term` varchar(32) NOT NULL COMMENT 'Срок доставки',
  `description` text NOT NULL COMMENT 'Краткое oписание',
  `created` datetime NOT NULL COMMENT 'Дата создания',
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Статус',
  `full_description` text NOT NULL COMMENT 'Полное описание',
  `need_phone` tinyint(3) unsigned NOT NULL COMMENT 'Телефон',
  `need_address` tinyint(3) unsigned NOT NULL COMMENT 'Адрес',
  `full_description_mobile` text NOT NULL COMMENT 'Полное описание (мобильное)',
  `default` tinyint(3) unsigned NOT NULL COMMENT 'Метод доставки по умолчанию',
  `need_place` tinyint(3) unsigned NOT NULL COMMENT 'Самовывоз',
  PRIMARY KEY (`id`),
  KEY `created_index` (`created`),
  KEY `order_index` (`order`),
  KEY `status_index` (`status`),
  KEY `title_index` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_methods_relations`
--

DROP TABLE IF EXISTS `orders_deliveries_methods_relations`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_methods_relations` (
  `name` enum('cities','payment_methods','services') NOT NULL COMMENT 'Имя',
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `record_from_id` mediumint(8) unsigned NOT NULL,
  `record_to_id` mediumint(8) unsigned NOT NULL,
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  `relationmanagment_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_record_to_id_record_from_id_unique` (`name`,`record_to_id`,`record_from_id`),
  KEY `record_from_id_index` (`record_from_id`),
  KEY `record_to_id_index` (`record_to_id`),
  KEY `order_index` (`order`),
  KEY `relationmanagment_id_index` (`relationmanagment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=102 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_services`
--

DROP TABLE IF EXISTS `orders_deliveries_services`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_services` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `contact_name` varchar(255) NOT NULL COMMENT 'Контактное лицо',
  `contact_phone` bigint(20) unsigned NOT NULL COMMENT 'Контактный номер',
  `address` varchar(255) NOT NULL COMMENT 'Адрес представительства в городе магазина',
  PRIMARY KEY (`id`),
  KEY `title_index` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_services_places`
--

DROP TABLE IF EXISTS `orders_deliveries_services_places`;
CREATE TABLE IF NOT EXISTS `orders_deliveries_services_places` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `city_id` mediumint(8) unsigned NOT NULL COMMENT 'ID города',
  `service_id` smallint(5) unsigned NOT NULL,
  `address` varchar(255) NOT NULL COMMENT 'Адрес',
  `url` varchar(255) NOT NULL COMMENT 'Ссылка на расположение склада на яндекс-картах',
  `longitude` decimal(9,6) unsigned DEFAULT NULL COMMENT 'Долгота',
  `latitude` decimal(8,6) unsigned DEFAULT NULL COMMENT 'Широта',
  `phones` varchar(255) NOT NULL COMMENT 'Контактные номера телефонов',
  `operating_schedule` text NOT NULL COMMENT 'График работы',
  `status` tinyint(3) unsigned NOT NULL COMMENT 'Статус',
  `order` mediumint(8) unsigned NOT NULL COMMENT 'Порядок',
  PRIMARY KEY (`id`),
  KEY `title_index` (`title`),
  KEY `service_id_index` (`service_id`),
  KEY `status_index` (`status`),
  KEY `order_index` (`order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` tinyint(3) unsigned NOT NULL,
  `is_section` tinyint(3) unsigned NOT NULL COMMENT 'Тип записи',
  `mpath` varchar(128) NOT NULL COMMENT 'Материальный путь',
  `title` varchar(255) NOT NULL COMMENT 'Название',
  `title_ua` varchar(255) NOT NULL COMMENT 'Название (по-украински)',
  `name` varchar(128) NOT NULL COMMENT 'Имя',
  `code` varchar(2) NOT NULL COMMENT 'Код страны',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `parent_id_index` (`parent_id`),
  KEY `is_section_index` (`is_section`),
  KEY `mpath_index` (`mpath`),
  KEY `title_index` (`title`),
  KEY `title_ua_index` (`title_ua`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `parent_id`, `is_section`, `mpath`, `title`, `title_ua`, `name`, `code`) VALUES
(1, 0, 0, '.', 'Украина', 'Україна', 'ukraine', '');


--
-- Table structure for table `countries_cities`
--

-- TRUNCATE TABLE `countries_cities`;
--
-- Dumping data for table `countries_cities`
--

INSERT INTO `countries_cities` (`id`, `parent_id`, `is_section`, `mpath`, `title`, `title_ua`, `name`, `country_id`, `type`, `codes`, `order`, `title_prepositional`, `ext_id`, `status`) VALUES
(25, 0, 0, '.', 'Киев', 'в', '25', 1, 4, '', 16777215, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_methods`
--

-- TRUNCATE TABLE `orders_deliveries_methods`;

--
-- Dumping data for table `orders_deliveries_methods`
--

INSERT INTO `orders_deliveries_methods` (`id`, `title`, `name`, `commission`, `term`, `description`, `created`, `order`, `status`, `full_description`, `need_phone`, `need_address`, `full_description_mobile`, `default`, `need_place`) VALUES
(21, 'Новая почта', '21', NULL, '', '', '2015-12-17 10:48:46', 16777215, 1, '', 1, 0, '', 0, 1),
(22, 'Meest Express', '22', NULL, '', '', '2015-12-17 16:37:33', 16777215, 1, '', 1, 0, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_methods_relations`
--

-- TRUNCATE TABLE `orders_deliveries_methods_relations`;

--
-- Dumping data for table `orders_deliveries_methods_relations`
--

INSERT INTO `orders_deliveries_methods_relations` (`name`, `id`, `record_from_id`, `record_to_id`, `order`, `relationmanagment_id`) VALUES
('services', 103, 22, 7, 0, NULL),
('services', 102, 21, 6, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_services`
--

-- TRUNCATE TABLE `orders_deliveries_services`;

--
-- Dumping data for table `orders_deliveries_services`
--

INSERT INTO `orders_deliveries_services` (`id`, `title`, `contact_name`, `contact_phone`, `address`) VALUES
(7, 'Meest Express', '1', 2, '3'),
(6, 'Новая почта', '1', 2, '3');

-- --------------------------------------------------------

--
-- Table structure for table `orders_deliveries_services_places`
--

-- TRUNCATE TABLE `orders_deliveries_services_places`;


--
-- Dumping data for table `orders_deliveries_services_places`
--

INSERT INTO `orders_deliveries_services_places` (`id`, `title`, `city_id`, `service_id`, `address`, `url`, `longitude`, `latitude`, `phones`, `operating_schedule`, `status`, `order`) VALUES
(19, 'Meest Express, Киев, ааа', 23, 7, 'ааа', '', NULL, NULL, '', '', 1, 16777215),
(17, 'Новая почта, Киев, yhjgf', 25, 6, 'yhjgf', '', NULL, NULL, '', '', 1, 16777215),
(18, 'Новая почта, Киев, ааа', 25, 6, 'ааа', '', NULL, NULL, '', '', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
